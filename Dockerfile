FROM python:3.9-slim

ENV PYTHONBUFFERED 1

RUN mkdir /ragnarok
WORKDIR /ragnarok

#RUN apt-get update && apt-get install -y gcc python3-dev
RUN pip install poetry

COPY . /ragnarok/
RUN poetry config virtualenvs.create false
RUN poetry install

